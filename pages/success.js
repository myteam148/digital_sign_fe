import React, { useEffect } from 'react'

function Success() {

    useEffect(() => {
        window.alert("Success")
    }, [])

  return (
    <div>Verify Successfully!</div>
  )
}

export default Success